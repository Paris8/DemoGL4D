# [Démo GL4D](https://www.api8.fr/)

<details><summary>Informations UP8</summary>

|                     |                                       |
|--------------------:|---------------------------------------|
|                 Nom | Anri KENNEL                           |
|              Classe | L2-X                                  |
|   Numéro d'étudiant | 20010664                              |
|                Mail | anri.kennel@etud.univ-paris8.fr       |
| Cycle universitaire | 2021-2022                             |
|              Module | Synthèse d'images, animation, et sons |
</details>

## [But](https://www.api8.fr/?sec=1)
Développer une démo d'une durée allant de 30 secondes à 4 minutes alliant bande sonore, synthèse d'images et animation.

## Utilisation
### Récupération et compilation
Cloner le programme avec soit :
- En SSH : `git clone git@code.up8.edu:Anri/demogl4d.git`
- En HTTPS : `git clone https://git.kennel.ml/Paris8/DemoGL4D.git`

Une fois dans le projet, pour compiler le programme, utilisez `make`.

> **Dépendances**
> - `SDL_mixer` (paquet Debian : `libsdl2-mixer-dev`, Arch : `sdl2_mixer`)

> `make release` pour compiler le programme avec les optimisations `O3`.

### Utilisation
- `./demo` lance la démo.

---
## Infos
- [Makefile](Makefile) de Farès BELHADJ.
- Basé sur les TPs fait tout le long de l'année
