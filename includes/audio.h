/* TP7 */

#ifndef DEMO_AUDIO_HPP
#define DEMO_AUDIO_HPP 1

#include <SDL_mixer.h>

// Charge et lance la musique
void initialisationMusique(Mix_Music *, const char *);

// Renvoie une moyenne de l'intensité du son
float moyenneMusique(void);

// Libère la musique en mémoire
void liberationMusique(Mix_Music *);

#endif
