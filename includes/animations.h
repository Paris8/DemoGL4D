#ifndef DEMO_ANIMATION_HPP
#define DEMO_ANIMATION_HPP 1

#include <GL4D/gl4dh.h>

#include "audio.h"

// Cube coloré qui tourne sur lui-même
void cube(int);

// Voronoi GPU
void voronoi(int);

// Formes en jouant avec caméra et polygones
void polygone(int);

// Kirby
void kirby(int);

// Fait tourner Kirby
void kirbyRotation(int);

// Dos de Kirby
void kirbyCredits(int);

#endif
