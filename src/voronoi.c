/* TP6 */

#include "../includes/animations.h"

// Initialise le diagramme de Voronoi
void diag_init(int, GLfloat **, GLfloat **);

void voronoi(int etat) {
    static GLuint quad = 0, proc_id = 0;
    static GLfloat *coords = NULL, *colors = NULL;
    static int sites_affiches = 1;
    const int nb_sites = 400;

    switch(etat) {
        case GL4DH_INIT:
            quad = gl4dgGenQuadf();
            proc_id = gl4duCreateProgram("<vs>shaders/voronoi.vs", "<fs>shaders/voronoi.fs", NULL);

            diag_init(nb_sites, &coords, &colors);
            break;

        case GL4DH_DRAW:
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glUseProgram(proc_id);

            if(sites_affiches > nb_sites) {
                sites_affiches = 1;
            }

            glUniform2fv(glGetUniformLocation(proc_id, "coords"), sites_affiches, coords);
            glUniform1i(glGetUniformLocation(proc_id, "nb_sites"), sites_affiches);
            glUniform4fv(glGetUniformLocation(proc_id, "colors"), nb_sites, colors);

            gl4dgDraw(quad);

            glUseProgram(0);

            ++sites_affiches;
            break;

        case GL4DH_FREE:
            if(coords != NULL) {
                free(coords);
                coords = NULL;
            }
            if(colors != NULL) {
                free(colors);
                colors = NULL;
            }
            break;

        default:
            break;
    }
}

void diag_init(int n, GLfloat **coords, GLfloat **colors) {
    *coords = malloc((GLuint64)n * 2 * sizeof(**coords));
    assert(*coords);

    *colors = malloc((GLuint64)n * 4 * sizeof(GLfloat));
    assert(*colors);

    for(int i = 0; i < n; ++i) {
        (*coords)[i * 2]     = gl4dmURand();
        (*coords)[i * 2 + 1] = gl4dmURand();

        (*colors)[i * 4]     = gl4dmURand();
        (*colors)[i * 4 + 1] = gl4dmURand();
        (*colors)[i * 4 + 2] = gl4dmURand();
        (*colors)[i * 4 + 3] = 1.;
    }
}
