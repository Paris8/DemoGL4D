/* TP7 */

#include "../includes/audio.h"

// Stocke les données à propos de la musique
static float moyenne;

float moyenneMusique(void) {
    return moyenne;
}

void callback(void *_, Uint8 *stream, int taille) {
    (void)_;

    int i = 0;
    double moyenne_calcul = 0.;
    for(signed short *stream16 = (signed short *)stream; i < taille / 2; ++i) {
        moyenne_calcul += stream16[i] / ((1 << 15) - 1.);
    }
    moyenne = fabs(moyenne_calcul / i);
}

void initialisationMusique(Mix_Music *musique, const char *filename) {
    Mix_Init(MIX_INIT_MID);
    Mix_OpenAudio(44100, AUDIO_S16LSB, 2, 1024);
    musique = Mix_LoadMUS(filename);

    Mix_VolumeMusic(MIX_MAX_VOLUME / 10);
    Mix_SetPostMix(callback, NULL);

    Mix_PlayMusic(musique, 1);
}

void liberationMusique(Mix_Music *musique) {
    if(musique) {
        if(Mix_PlayingMusic()) {
            Mix_HaltMusic();
        }
        Mix_FreeMusic(musique);
        musique = NULL;
    }

    Mix_CloseAudio();
    Mix_Quit();
}
