/* TP4 */

#include "../includes/animations.h"

void polygone(int etat) {
    static GLuint proc_id = 0, cube = 0, sphere = 0, tore = 0;
    GLint color = 0;
    static double couleurs[6];

    static GLfloat a = 0, b = 0;

    switch(etat) {
        case GL4DH_INIT:
            cube   = gl4dgGenCubef();
            sphere = gl4dgGenSpheref(10, 10);
            tore   = gl4dgGenTorusf(15, 15, 1.5);

            proc_id = gl4duCreateProgram("<vs>shaders/polygone.vs", "<fs>shaders/polygone.fs", NULL);

            gl4duGenMatrix(GL_FLOAT, "modview");
            gl4duGenMatrix(GL_FLOAT, "proj");

            for(int i = 0; i < 6; ++i) {
                couleurs[i] = gl4dmURand();
            }
            break;

        case GL4DH_DRAW:
            glClearColor(.0f, .0f, .5f, 1.f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glUseProgram(proc_id);

            gl4duBindMatrix("proj");
            gl4duLoadIdentityf();
            gl4duFrustumf(-1, 1, -0.63, 0.63, 1, 1000);

            gl4duBindMatrix("modview");
            gl4duLoadIdentityf();
            gl4duTranslatef(0, 0, -5);

            for(int i = 0; i < 2; ++i) {
                gl4duPushMatrix();
                gl4duRotatef(a, 0, 1, 0);
                if(i % 2) {
                    gl4duRotatef(a, 0, 0, 1);
                } else {
                    gl4duRotatef(a, 1, 0, 0);
                }
                color = glGetUniformLocation(proc_id, "couleur");
                gl4duLookAtf(1, 1, 1, 0, 0, 0, 0, 0, 1);
                glUniform3f(color, couleurs[i], couleurs[i + 1], couleurs[i + 2]);
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                gl4duSendMatrix();
                gl4dgDraw(cube);
                gl4duPopMatrix();
            }

            gl4duPushMatrix();
            gl4duRotatef(b, 0, 1, 0);
            color = glGetUniformLocation(proc_id, "couleur");
            glUniform3f(color, couleurs[2], couleurs[3], couleurs[4]);
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            gl4duSendMatrix();
            gl4dgDraw(sphere);
            gl4duPopMatrix();

            gl4duPushMatrix();
            gl4duRotatef(a + b, 0, 1, 0);
            color = glGetUniformLocation(proc_id, "couleur");
            glUniform3f(color, couleurs[3], couleurs[4], couleurs[5]);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            gl4duSendMatrix();
            gl4dgDraw(tore);
            gl4duPopMatrix();
            gl4duSendMatrices();

            glUseProgram(0);

            a += 1;
            b += 2;
            break;

        default:
            break;
    }
}
