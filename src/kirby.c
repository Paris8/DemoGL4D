/* TP 5 */

#include "../includes/animations.h"

#include <SDL_image.h>

void kirby(int etat) {
    static GLuint boule = 0, proc_id = 0, tex_id = 0;
    static GLfloat musique[3] = {0};

    switch(etat) {
        case GL4DH_INIT:
            boule = gl4dgGenSpheref(20, 20);

            SDL_Surface *image = IMG_Load("images/kirby.jpg");
            SDL_Surface *surface = SDL_CreateRGBSurface(0, image->w, image->h, 32, R_MASK, G_MASK, B_MASK, A_MASK);
            SDL_BlitSurface(image, NULL, surface, NULL);
            SDL_FreeSurface(image);

            glGenTextures(1, &tex_id);
            glBindTexture(GL_TEXTURE_2D, tex_id);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, surface->pixels);
            SDL_FreeSurface(surface);
            glBindTexture(GL_TEXTURE_2D, 0);

            proc_id = gl4duCreateProgram("<vs>shaders/kirby.vs", "<fs>shaders/kirby.fs", NULL);

            gl4duGenMatrix(GL_FLOAT, "proj");
            gl4duGenMatrix(GL_FLOAT, "view");

            glEnable(GL_DEPTH_TEST);
            break;

        case GL4DH_DRAW:
            glClearColor(.66f, .17f, .24f, 1.f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            glUseProgram(proc_id);

            gl4duBindMatrix("proj");
            gl4duLoadIdentityf();
            gl4duFrustumf(-1.f, 1.f, -.57f, .57f, 1.f, 1000.f);

            gl4duBindMatrix("view");
            gl4duLoadIdentityf();

            gl4duLookAtf(2.f, 0.f, 2.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f);

            gl4duPushMatrix();

            gl4duRotatef(135, 0, 1, 0);

            GLfloat s = moyenneMusique() * 5000.f;
            if(musique[0] != 0 || musique[1] != 0) {
                s = (s - musique[0]) / (musique[1] - musique[0]) + .2f;
                if(s > musique[2]) {
                    s -= s / 7.f;
                }
                if(s < musique[2]) {
                    s += s / 7.f;
                }
            }
            musique[1] = musique[1] < s ? s : musique[1];
            musique[0] = musique[0] > s ? s : musique[0];
            musique[2] = s;

            gl4duPushMatrix();
            if(musique[1] != musique[0]) {
                gl4duScalef(s, s, s);
            }

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            gl4duSendMatrices();

            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, tex_id);
            glUniform1i(glGetUniformLocation(proc_id, "tex"), 0);

            gl4dgDraw(boule);
            gl4duPopMatrix();

            glUseProgram(0);
            break;

        default:
            break;
    }
}
