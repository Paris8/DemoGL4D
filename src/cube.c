/* TP 6 et 7 */

#include "../includes/animations.h"

void cube(int etat) {
    static GLuint cube = 0, proc_id = 0;
    static GLclampf rgb[3] = { 0 };
    static GLfloat musique[3] = {0};

    switch(etat) {
        case GL4DH_INIT:
            for(int i = 0; i < 3; ++i) {
                rgb[i] = gl4dmURand();
            }

            cube = gl4dgGenCubef();
            proc_id = gl4duCreateProgram("<vs>shaders/cube.vs", "<fs>shaders/cube.fs", NULL);

            gl4duGenMatrix(GL_FLOAT, "modview");
            gl4duGenMatrix(GL_FLOAT, "view");
            gl4duGenMatrix(GL_FLOAT, "proj");

            glEnable(GL_DEPTH_TEST);
            break;

        case GL4DH_DRAW:
            glClearColor(rgb[0], rgb[1], rgb[2], 1.f);
            static GLfloat cube_rotation = 0;
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glUseProgram(proc_id);

            gl4duBindMatrix("proj");
            gl4duLoadIdentityf();
            gl4duFrustumf(-1.f, 1.f, -.57f, .57f, 1.f, 1000.f);

            gl4duBindMatrix("view");
            gl4duLoadIdentityf();
            gl4duLookAtf(0, 0, 5, 0, 0, 0, 0, 1, 0);

            gl4duBindMatrix("modview");
            gl4duLoadIdentityf();

            GLfloat s = moyenneMusique() * 5000.f;
            if(musique[0] != 0 || musique[1] != 0) {
                s = (s - musique[0]) / (musique[1] - musique[0]) + .5f;
                if(s > musique[2]) {
                    s -= s / 10.f;
                }
                if(s < musique[2]) {
                    s += s / 10.f;
                }
            }
            musique[1] = musique[1] < s ? s : musique[1];
            musique[0] = musique[0] > s ? s : musique[0];
            musique[2] = s;

            gl4duPushMatrix();
            if(musique[1] != musique[0]) {
                gl4duScalef(s, s, s);
            }
            gl4duRotatef(cube_rotation, 0, 1, 0);
            gl4duSendMatrices();
            gl4dgDraw(cube);
            gl4duPopMatrix();

            glUseProgram(0);

            ++cube_rotation;
            break;

        default:
            break;
    }
}
