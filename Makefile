# Auteur : Farès BELHADJ (amsi@up8.edu)

# Définition des commandes utilisées
CC    = gcc
ECHO  = echo
RM    = rm -f
TAR   = tar
ZIP   = zip
MKDIR = mkdir
CHMOD = chmod
CP    = rsync -R

# Déclaration des options du compilateur
CFLAGS   =
CPPFLAGS = -I.
LDFLAGS  = -lm

# Définition des fichiers et dossiers
PACKNAME   = projet
PROGNAME   = demo
VERSION    = 1.0
distdir    = $(PACKNAME)_$(PROGNAME)-$(VERSION)
HEADERS    = $(wildcard includes/*.h)
SOURCES    = window.c $(wildcard src/*.c)
OBJ        = $(SOURCES:.c=.o)
DOXYFILE   = documentation/Doxyfile
EXTRAFILES = COPYING $(wildcard shaders/*.?s images/*)
DISTFILES  = $(SOURCES) Makefile $(HEADERS) $(DOXYFILE) $(EXTRAFILES)

# Traitements automatiques pour ajout des chemins et options
ifneq (,$(shell ls -d /usr/local/include 2>/dev/null | tail -n 1))
	CPPFLAGS += -I/usr/local/include
endif
ifneq (,$(shell ls -d $(HOME)/local/include 2>/dev/null | tail -n 1))
	CPPFLAGS += -I$(HOME)/local/include
endif
ifneq (,$(shell ls -d /usr/local/lib 2>/dev/null | tail -n 1))
	LDFLAGS += -L/usr/local/lib
endif
ifneq (,$(shell ls -d $(HOME)/local/lib 2>/dev/null | tail -n 1))
	LDFLAGS += -L$(HOME)/local/lib
endif
ifeq ($(shell uname),Darwin)
	MACOSX_DEPLOYMENT_TARGET = 10.8
        CFLAGS += -mmacosx-version-min=$(MACOSX_DEPLOYMENT_TARGET)
        LDFLAGS += -framework OpenGL -mmacosx-version-min=$(MACOSX_DEPLOYMENT_TARGET)
else
        LDFLAGS += -lGL
endif

CPPFLAGS += $(shell sdl2-config --cflags)
LDFLAGS  += -lGL4Dummies $(shell sdl2-config --libs) -lSDL2_mixer -lSDL2_image

%.o: %.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

# Version finale avec optimisations (défaut)
$(PROGNAME): CFLAGS += -O3
$(PROGNAME): compilation

# Débug
debug: CFLAGS += -Wall -Wextra -Wshadow -pedantic -g -Wsign-conversion -fanalyzer
debug: compilation

compilation: $(OBJ)
	$(CC) $(OBJ) $(LDFLAGS) -o $(PROGNAME)

dist: distdir
	$(CHMOD) -R a+r $(distdir)
	$(TAR) zcvf $(distdir).tgz $(distdir)
	$(RM) -r $(distdir)

zip: distdir
	$(CHMOD) -R a+r $(distdir)
	$(ZIP) -r $(distdir).zip $(distdir)
	$(RM) -r $(distdir)

distdir: $(DISTFILES)
	$(RM) -r $(distdir)
	$(MKDIR) $(distdir)
	$(CHMOD) 777 $(distdir)
	$(CP) $(DISTFILES) $(distdir)

doc: $(DOXYFILE)
	cat $< | sed -e "s/PROJECT_NAME *=.*/PROJECT_NAME = $(PROGNAME)/" |\
	  sed -e "s/PROJECT_NUMBER *=.*/PROJECT_NUMBER = $(VERSION)/" >> $<.new
	mv -f $<.new $<
	cd documentation && doxygen && cd ..

all: $(PROGNAME)

clean:
	@$(RM) -r $(PROGNAME) $(OBJ) *~ $(distdir).tgz $(distdir).zip gmon.out	\
	  core.* documentation/*~ shaders/*~ documentation/html
