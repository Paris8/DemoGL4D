/* TP4 */

#version 330

uniform vec3 couleur;

out vec4 fragColor;

void main(void) {
    fragColor = vec4(couleur, 1.);
}
