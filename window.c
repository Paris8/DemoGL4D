/* TP8 */

#include <GL4D/gl4duw_SDL2.h>

#include "includes/animations.h"

// Son de fond
static Mix_Music *ambiance = NULL;

// Comportement à la fermeture du programme
void fermeture(void) {
    liberationMusique(ambiance);
    gl4duClean(GL4DU_ALL);
}

int main(int argc, char *argv[]) {
    // Dimensions initiales de la fenêtre
    int dimensions[] = { 1280, 720 };

    if(!gl4duwCreateWindow(argc, argv, "Démo Anri KENNEL", 10, 10, dimensions[0], dimensions[1], GL4DW_SHOWN)) {
        return 1;
    }

    initialisationMusique(ambiance, "audio/ambiance.mid");

    // Animations
    GL4DHanime animations[] = {
        { 6800,          cube, NULL, NULL },
        { 8500,       voronoi, NULL, NULL },
        { 6000,      polygone, NULL, NULL },
        { 5000,         kirby, NULL, NULL },
        { 1700, kirbyRotation, NULL, NULL},
        { 3000, kirbyCredits, NULL, NULL},
        {    0,     NULL, NULL, NULL }
    };

    gl4dhInit(animations, dimensions[0], dimensions[1], NULL);
    atexit(fermeture);

    gl4duwDisplayFunc(gl4dhDraw);
    gl4duwMainLoop();

    return 0;
}
